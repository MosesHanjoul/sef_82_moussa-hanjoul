<?php
echo "How many games do you want to play today ?\n";
$userNumber=(trim(fgets(STDIN)));

class GameGenerator {
	private $FIRSTLIST = array(25 ,50 , 75,100);
	private $SECONDLIST = array(1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10);
    public $TARGET;

	function __construct() {

	}

	function setTarget() {
		$this->TARGET=rand(101,999);
	}

	public function getTarget() {
		echo "\nThe Target is: {$this->TARGET}\n";
		return $this->TARGET;
	}

	function holder() 
	{
		$gameInput=array();
		$firstClone=$this->FIRSTLIST;
		$secondClone=$this->SECONDLIST;
		$comparator=rand(1,4);

		for ($i=0 ; $i<$comparator ; $i++) {
			$index1=array_rand($firstClone,1);
			$gameInput[$i]=$firstClone[$index1];
			unset($firstClone[$index1]);
		}

		for ($i=0 ; $i<6-$comparator ; $i++) {
			$index2=array_rand($secondClone,1);
			array_push($gameInput,$secondClone[$index2]);
			unset($secondClone[$index2]);
		}
		echo "The Number Combination is: ";
		foreach ($gameInput as $inputValue) {
			echo "$inputValue ";
		}
		return $gameInput;	
	}
}
