<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post_path', 'post_title', 'post_description', 'post_like', 'user_id'
    ];

    public function user() 
    {
    	return $this->belongsTo('User');
    }

}
