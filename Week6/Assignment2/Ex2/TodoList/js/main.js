
var session = (localStorage.getItem('todolist')) ? JSON.parse(localStorage.getItem('todolist')) : {
	itemTodo: [],
	itemDone: []
};

loadStorage();



document.getElementById('add').addEventListener('click', function ()
{
	var newValue = document.getElementById('newItem').value;
	var newDescrip = document.getElementById('description').value;
	if (newValue && newDescrip) {
		if (newValue.length < 50 && newDescrip.length < 100) {
			session.itemTodo.push(newValue);
			newTask(newValue, newDescrip);
			var newValue = document.getElementById('newItem').value = '';
			var newDescrip = document.getElementById('description').value = '';


		}else {

		}
	} 
})

function loadStorage()
{
	if (!session.itemTodo.length && !session.itemDone.length) {
		return;
	}
	for (var i=0; i < session.itemTodo.length; i++) {
		var value = session.itemTodo[i];
		newTask(value);
	}
	for (var j=0; j < session.itemDone.length; j++) {
		var value = session.itemTodo[j];
		newTask(value);
	}
}

function dataStorage()
{
	localStorage.setItem('todolist', JSON.stringify(session));
}

function newTask (text, note, itemDone) 
{
	var list = (itemDone) ? document.getElementById('itemDone') : document.getElementById('itemTodo');
	var date = getDate();

	var item = document.createElement('li');
	item.innerHTML = text + "<br/> &nbsp&nbsp" + 'Added : ' + date + "<br/> &nbsp&nbsp" + note;

	var buttons = document.createElement('div');
	buttons.classList.add('buttons');

	var remove = document.createElement('button');
	remove.classList.add('remove');
	remove.innerHTML = 'X';
	remove.addEventListener('click', removeItem);

	var complete = document.createElement('button');
	complete.classList.add('complete');
	complete.innerHTML = 'Done!';
	complete.addEventListener('click', completeItem);

	buttons.appendChild(remove);
	buttons.appendChild(complete);

	item.appendChild(buttons);
	list.insertBefore(item, list.childNodes[0]);

	dataStorage();
}

function getDate ()
{
	var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric',hour: 'numeric', minute: 'numeric'};
	var date = new Intl.DateTimeFormat('en-US', options).format(date);
	return (date);
}

function removeItem ()
{
	var item = this.parentNode.parentNode;
	var parent = item.parentNode;
	var id = parent.id;
	var value = item.innerText;

	if ( id === 'itemTodo') {
		session.itemTodo.splice(session.itemTodo.indexOf(value), 1);
	} else {
		session.itemDone.splice(session.itemDone.indexOf(value), 1);
	}

	parent.removeChild(item);
	dataStorage();
}

function completeItem ()
{
	var item = this.parentNode.parentNode;
	var parent = item.parentNode;
	var id = parent.id;
	var value = item.innerText;

	if ( id === 'itemTodo') {
		session.itemTodo.splice(session.itemTodo.indexOf(value), 1);
		session.itemDone.push(value);
	} else {
		session.itemDone.splice(session.itemDone.indexOf(value), 1);
		session.itemTodo.push(value);
	}


	var target = ( id === 'itemTodo') ? document.getElementById('itemDone') : document.getElementById('itemTodo');

	parent.removeChild(item);
	target.insertBefore(item, target.childNodes[0]);
	dataStorage();
}

