var source = {"ID":document.getElementById("pillar_1"), "margin": 270};
var auxilliary ={"ID":document.getElementById("pillar_2"), "margin": 430};	
var destination ={"ID": document.getElementById("pillar_3"), "margin": 430};

var first_disk = document.getElementById("first_disk");
var second_disk = document.getElementById("second_disk");
var third_disk = document.getElementById("third_disk");
var fourth_disk = document.getElementById("fourth_disk");
var fifth_disk = document.getElementById("fifth_disk");
var sixth_disk = document.getElementById("sixth_disk");
var seventh_disk = document.getElementById("seventh_disk");
var eighth_disk = document.getElementById("eighth_disk");

var disks = [first_disk, second_disk, third_disk, fourth_disk, fifth_disk, sixth_disk, seventh_disk, eighth_disk];
var time = 0;

function SelectDisk(diskID, source, destination) 
{
	var intervalID = setInterval(function MoveUp() 
	{
		var diskYAttrib = diskID.getAttribute("y");
		var animateY = parseInt(diskYAttrib) - 10;
		if (animateY < 50) {
			clearInterval(intervalID);
			source.margin += 20;
			MoveDisk(diskID, source, destination);
		}
		diskID.setAttribute("y", animateY);
	},20);
}
function PlaceDisk(diskID, source) 
{
	var intervalID = setInterval(function MoveDown() 
	{
		var diskYAttrib = diskID.getAttribute("y");
		var animateY = parseInt(diskYAttrib) + 10;
		if (animateY >= source.margin) {
			clearInterval(intervalID);
			source.margin -= 20;
		}
		diskID.setAttribute("y", animateY);
	},20);
}

function MoveDisk(diskID, source, destination)
{
	var intervalID = setInterval(function Move()
	{
		var sourceXAttrib = source.ID.getAttribute("x");
		var destinationXAttrib = destination.ID.getAttribute("x");
		var diskXAttrib = diskID.getAttribute("x");
		if (sourceXAttrib < destinationXAttrib) {
			var animateX = parseInt(diskXAttrib) + 10;
			if (animateX > destinationXAttrib - (diskID.getAttribute("width")/2)) {
				clearInterval(intervalID);
				PlaceDisk(diskID,destination);
			}
		diskID.setAttribute("x", animateX);
		} else {
			var animateX = parseInt(diskXAttrib) - 10;
			if (animateX < destinationXAttrib - (diskID.getAttribute("width")/2 - 20)) {
				clearInterval(intervalID);
				PlaceDisk(diskID,destination);
			}
		diskID.setAttribute("x", animateX);
		}
	},20);
}

function HanoiSolver(disk, source, destination, auxilliary) 
{
	if ( disk == 0 ) {
		setTimeout(SelectDisk,time,disks[disk], source, destination);
		time += 2000;
	} else {
		HanoiSolver(disk - 1, source, auxilliary, destination);
		setTimeout(SelectDisk,time,disks[disk],source, destination);
		time += 2000;
		HanoiSolver(disk - 1, auxilliary, destination, source);
	}
}

function Button() {
		HanoiSolver( 7, source, destination, auxilliary);
}
