CREATE DATABASE FinanceDB;

use FinanceDB;

CREATE TABLE IF NOT EXISTS FiscalYearTable (id int PRIMARY KEY AUTO_INCREMENT,
fiscal_year varchar(30) NOT NULL UNIQUE,start_date date NOT NULL,
end_date date NOT NULL);

delimiter @
CREATE TRIGGER FY_length
BEFORE INSERT ON FiscalYearTable FOR EACH ROW
    BEGIN
    IF ( DATEDIFF(NEW.end_date, NEW.start_date) > 365 ) THEN
SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'fiscal year is maximal length is 1 year';
        ELSEIF ( DATEDIFF(NEW.end_date, NEW.start_date) < 0 ) THEN
        SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = 'fiscal year is START date must be smaller than END date';
        END IF ;
    END;@