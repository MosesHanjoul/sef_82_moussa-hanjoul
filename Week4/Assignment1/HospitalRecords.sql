CREATE DATABASE HospitalRecords;

use HospitalRecords;
CREATE TABLE IF NOT EXISTS Procedures (proc_id INT(10), anest_name VARCHAR(20),
start_time time, end_time time);

INSERT INTO `Procedures`(`proc_id`, `anest_name`, `start_time`, `end_time`) VALUES ('1','`Albert`','08:00','11:00');
INSERT INTO `Procedures`(`proc_id`, `anest_name`, `start_time`, `end_time`) VALUES ('2','`Albert`','09:00','13:00');
INSERT INTO `Procedures`(`proc_id`, `anest_name`, `start_time`, `end_time`) VALUES ('3','`Kamal`','08:00','13:30');
INSERT INTO `Procedures`(`proc_id`, `anest_name`, `start_time`, `end_time`) VALUES ('4','`Kamal`','09:00','15:30');
INSERT INTO `Procedures`(`proc_id`, `anest_name`, `start_time`, `end_time`) VALUES ('5','`Kamal`','10:00','11:30');
INSERT INTO `Procedures`(`proc_id`, `anest_name`, `start_time`, `end_time`) VALUES ('6','`Kamal`','12:30','13:30');
INSERT INTO `Procedures`(`proc_id`, `anest_name`, `start_time`, `end_time`) VALUES ('7','`Kamal`','13:30','14:30');
INSERT INTO `Procedures`(`proc_id`, `anest_name`, `start_time`, `end_time`) VALUES ('8','`Kamal`','18:30','19:00');

CREATE VIEW Events (proc_id, comparison_proc, anest_name, 
event_time, event_type)

AS SELECT P1.proc_id,
          P2.proc_id,
          P1.anest_name,
          P2.start_time,
          +1
FROM Procedures AS P1, Procedures AS P2
WHERE P1.anest_name = P2.anest_name
AND NOT (P2.end_time <= P1.start_time  OR P2.start_time >= P1.end_time)
UNION
SELECT P1.proc_id,
       P2.proc_id,
       P1.anest_name,
       P2.end_time,
       -1 AS event_type  
FROM Procedures AS P1, Procedures AS P2
WHERE P1.anest_name = P2.anest_name
AND NOT (P2.end_time <= P1.start_time OR P2.start_time >= P1.end_time); 

SELECT E1.proc_id, E1.event_time,
  (SELECT SUM(E2.event_type)
  FROM Events AS E2
  WHERE E2.proc_id = E1.proc_id AND E2.event_time < E1.event_time)
  AS instantaneous_count
  FROM Events AS E1
  ORDER BY E1.proc_id, E1.event_time; 

SELECT E1.proc_id,
  MAX((SELECT SUM(E2.event_type)
  FROM Events AS E2
  WHERE E2.proc_id = E1.proc_id
  AND E2.event_time < E1.event_time)) AS max_inst_count
  FROM Events AS E1
  GROUP BY E1.proc_id;