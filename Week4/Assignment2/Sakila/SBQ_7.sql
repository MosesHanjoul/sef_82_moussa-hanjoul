use sakila;

(SELECT CustFullNames.first_name AS Match_FName, last_name AS Match_LName
FROM customer AS CustFullNames
JOIN 
(SELECT first_name
FROM actor
where actor_id=8) AS ID8Name
ON ID8Name.first_name = CustFullNames.first_name)
UNION
(SELECT ActorFullNames.first_name AS ActorFName, last_name AS ActorLName
FROM actor AS ActorFullNames
JOIN 
(SELECT first_name
FROM actor
where actor_id=8) AS ID8Name
ON ID8Name.first_name = ActorFullNames.first_name
WHERE actor_id <> 8);