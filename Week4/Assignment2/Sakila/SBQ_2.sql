use sakila;

SELECT name AS Language,number_of_movies,release_year
FROM language as l
JOIN
(SELECT COUNT(film_id) as number_of_movies,language_id,release_year
FROM film
WHERE release_year=2006
GROUP BY language_id
ORDER BY COUNT(film_id) DESC LIMIT 3) AS f
ON f.language_id = l.language_id;