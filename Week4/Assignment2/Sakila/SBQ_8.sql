use sakila;

SELECT store_id,
       Month(rental_date) AS month,
       Year(rental_date)  AS year,
       Sum(amount)        AS total,
       Avg(amount)        AS average
FROM payment 
AS PaymentInfo
RIGHT JOIN (SELECT store_id,
                   rental_id,
                   rental_date
            FROM customer AS CustomerInfo
            RIGHT JOIN (SELECT customer_id,
                               rental_id,
                               rental_date
                        FROM  rental
                        ORDER BY Year(rental_date),
                                 Month(rental_date)) AS RentalsInfo
            ON CustomerInfo.customer_id = RentalsInfo.customer_id) AS TotalRentalsInfo
ON PaymentInfo.rental_id = TotalRentalsInfo.rental_id
GROUP BY year, month, store_id
ORDER BY store_id, year, month;