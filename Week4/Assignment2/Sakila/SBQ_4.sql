use sakila;

SELECT address2 as Valid_2nd_Address
FROM address
WHERE address2 NOT IN ("NULL", "")
ORDER BY address2 ASC;