use sakila;

SELECT last_name, first_name, release_year 
FROM actor AS ActrYear 
JOIN (select actor_id, release_year
      FROM film_actor AS YearActrID
      JOIN (SELECT film_id, release_year
            FROM film
            WHERE description LIKE "%crocodile%"
            AND description LIKE "%shark%") AS YearFilmID
      		ON YearActrID.film_id = YearFilmID.film_id) AS ActrFilmID 
			ON ActrYear.actor_id = ActrFilmID.actor_id 
ORDER BY last_name ASC;