use sakila;

SELECT country as COUNTRY,COUNT(ID) as Number_of_Customers
FROM customer_list
GROUP BY country
ORDER BY COUNT(ID) DESC LIMIT 3;