CREATE TABLE `ClaimStatusCodes` (
  `claim_status` char(2) DEFAULT NULL,
  `claim_status_desc` varchar(40) DEFAULT NULL,
  `claim_seq` int(11) NOT NULL,
  PRIMARY KEY (`claim_seq`),
  UNIQUE KEY `claim_status` (`claim_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
