USE PatientClaimDB;

CREATE TABLE LegalEvents (
  claim_id int(11) NOT NULL,
  defendant_name varchar(20) DEFAULT NULL,
  claim_status char(2) DEFAULT NULL,
  change_date date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO LegalEvents VALUES
(1,"jean Skaff","AP","2016-01-01"),
(1,"jean Skaff","OR","2016-02-02"),
(1,"jean Skaff","SF","2016-03-01"),
(1,"jean Skaff","CL","2016-04-01"),
(1,"Radwan Sameh","AP","2016-01-01"),
(1,"Radwan Sameh","OR","2016-02-02"),
(1,"Radwan Sameh","SF","2016-03-01"),
(1,"Elie Meouchi","AP","2016-01-01"),
(1,"Elie Meouchi","OR","2016-02-02"),
(2,"Radwan Sameh","AP","2016-01-01"),
(2,"Radwan Sameh","OR","2016-02-01"),
(2,"Paul Syoufi","AP","2016-01-01"),
(3,"Issam Awwad","AP","2016-01-01");