USE PatientClaimDB;

CREATE TABLE Claims (
    claim_id INT(11) NOT NULL,
    patient_name VARCHAR(20) DEFAULT NULL,
    PRIMARY KEY (claim_id),
    UNIQUE KEY claim_id (claim_id)
)  ENGINE=INNODB DEFAULT CHARSET=LATIN1;

INSERT INTO Claims VALUES 
('1','Bassem Dghaidi'),
('2','Omar Breidi'),
('3','Marwan Sawwan');