USE PatientClaimDB;

CREATE TABLE Defendants (
  claim_id int(11) NOT NULL,
  defendant_name varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO Defendants VALUES
(1,"Jean Skaff"),
(1,"Elie Meouchi"),
(1,"Radwan Sameh"),
(2,"Joseph Eid"),
(2,"Paul Syoufi"),
(2,"Radwan Sameh"),
(3,"Issam Awwad");
