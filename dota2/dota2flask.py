import json
import pprint
from flask_cors import CORS
from flask import Flask
import dota2api
api = dota2api.Initialise("59B6B411F25EEAF03CB871DF0503E184")

app = Flask(__name__)
CORS(app)

@app.route('/match/<match_id>')
def match(match_id):
    # hist = api.get_match_history(match_id=41231571)
    match = api.get_match_details(match_id=match_id)
    return json.dumps(match)

@app.route('/hist/<player_id>')
def hist(player_id):
    #hist = api.get_match_history(account_id=76482434)
    hist = api.get_match_history(account_id=player_id)
    return json.dumps(hist)

@app.route('/playerinfo/<player_id>')
def pinfo(player_id):
    pinfo = api.get_player_summaries(account_id=player_id)
    return json.dumps(pinfo)
