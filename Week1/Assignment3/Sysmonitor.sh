free -m | awk '/Mem:/ {average=(($3/$2)*100) ;
 if (average > 80) 
 	{
 		print "Alarm : virtual memory is at: "average;
    }
else
    {print "Everything is OK";}
}'

df --total | awk '/sda1/ {diskavr=(($3/$2)*100) ;
if (diskavr > 80) 
 	{
 		print "Alarm : Disk/dev/sda1 is at: "diskavr;
    }
else
    {print "Everything is OK";}
}'

 
