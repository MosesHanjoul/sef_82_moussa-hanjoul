sol=/home/moses/Desktop/log_dump.csv

if [ -f $sol ] ; then
 	rm $sol
fi

for file in /var/log/*.log
do
  FILENAME=`basename $file`
  FILESIZE=`sudo du -k $file | cut -f1`
  echo $FILENAME  ","  $FILESIZE >> $sol
done

