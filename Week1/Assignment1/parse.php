<?php 
    $line=file("/var/log/apache2/access.log");

    foreach($line as $value)
    {
        $first_comma=strpos($value, '"');
        $second_comma=strpos($value, '"', $first_comma+1)-$first_comma;
        $first_space_after_second_comma=strpos($value, " ", $first_comma+$second_comma+1);
        $first_space=strpos($value, ' ');
        $open_bracket=strpos($value, '[');
        $plus=strpos($value, '+');
        
        echo substr($value, 0, $first_space)." -- ";
        echo substr($value, $open_bracket+1, $plus-$open_bracket-1)." -- ";

        echo substr($value, $first_comma+1, strpos($value, '"', $first_comma+1)-$first_comma-1)." -- ";    

        echo substr($value, $first_space_after_second_comma+1, strpos($value, " ", $first_space_after_second_comma+1)-$first_space_after_second_comma-1)."\n";
    }


    echo $date->format('l, F d Y : H-i-s');

?>