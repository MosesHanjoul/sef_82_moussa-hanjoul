$(document).ready(function() {
var logicController = {

  apiURL: 'http://localhost:5000/',
  games: '',

  init: function() {
    this.loadData(this.apiURL);
  },

  loadData: function(apiURL) {
    $.ajax({
      context: this,
      type: 'GET',
      dataType: "json",
      url: this.apiURL + 'hist/' + {{ $steamID }},
      success: function(data) {
        console.log(data);
      },
    });
  },
};

logicController.init();

});