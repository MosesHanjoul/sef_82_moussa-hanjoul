<?php

namespace WildCard\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index() {
        $steamId = Auth::User()->steamid32;
        return view('dashboard')
            ->with(['steamID' => $steamId]);
    }
}
