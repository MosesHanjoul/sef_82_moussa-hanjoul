<?php

namespace WildCard\Http\Controllers;

use Invisnik\LaravelSteamAuth\SteamAuth;
use WildCard\User;
use Auth;

class AuthController extends Controller
{
    /**
     * The SteamAuth instance.
     *
     * @var SteamAuth
     */
    protected $steam;

    /**
     * The redirect URL.
     *
     * @var string
     */
    protected $redirectURL = '/dashboard';

    /**
     * AuthController constructor.
     * 
     * @param SteamAuth $steam
     */
    public function __construct(SteamAuth $steam)
    {
        $this->steam = $steam;
    }

    /**
     * Redirect the user to the authentication page
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function redirectToSteam()
    {
        return $this->steam->redirect();
    }

   

    /**
     * Get user info and log in
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function handle()
    {
        if ($this->steam->validate()) {
            $info = $this->steam->getUserInfo();

            if (!is_null($info)) {
                $user = $this->findOrNewUser($info);

                Auth::login($user, true);

                return redirect($this->redirectURL); // redirect to site
            }
        }
        return $this->redirectToSteam();
    }
    //convert steamID64 into SteamID32
    function convert_steamid_64bit_to_32bit($id)
    {
        $result = substr($id, 3) - 61197960265728;
        return (string) $result;
    }

    /**
     * Getting user by info or created if not exists
     *
     * @param $info
     * @return User
     */
    protected function findOrNewUser($info)
    {
        $user = User::where('steamid64', $info->steamID64)->first();
        
        if (!is_null($user)) {
            return $user;
        
        }

        
        return User::create([
            'name' => $info->personaname,
            'avatar' => $info->avatar,
            'avatarmedium' => $info->avatarmedium,
            'avatarfull' => $info->avatarfull,
            'steamid64' => $info->steamID64,
            'steamid32' => $this -> convert_steamid_64bit_to_32bit($info->steamID64)
        ]);
    }
}