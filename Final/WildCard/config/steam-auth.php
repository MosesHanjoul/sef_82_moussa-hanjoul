<?php

return [

    /*
     * Redirect URL after login
     */
    'redirect_url' => '/auth/steam/handle',
    /*
     * API Key (set in .env file) [http://steamcommunity.com/dev/apikey]
     */
    'api_key' => env('STEAM_API_KEY', '59B6B411F25EEAF03CB871DF0503E184'),
    /*
     * Is using https ?
     */
    'https' => false

];
