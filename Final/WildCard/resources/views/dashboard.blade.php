<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('css/dashboard.css') }}" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Allerta Stencil' rel='stylesheet'>
    <link href="https://fonts.googleapis.com/css?family=Orbitron" rel="stylesheet">
    
    <title>{{config('app.name')}}</title>
  </head>
  <body>
    <div class="container">

      <div class="row header-container">
          <div class="col-lg-4">
            <div class="dashboard-logo">DashBoard</div>
          </div>
          <div class="wildcard-header">
            <div class="col-lg-4">{{config('app.name')}}</div>
          </div>
            <div class="col-lg-4">
              <div class="logout-btn pull-right">
                <a href="logout">Logout</a>
              </div>
              <div class="avatar-small pull-right">
                <img src="{{Auth::user()->avatarmedium}}">
              </div>
            </div>
        </div>

        <div class="row"><br /></div>

        <div class="row body-container">

          <div class="col-lg-2">
            <div class="user_profile">
              <div class="avatar-medium">
              <img src="{{Auth::user()->avatarfull}}">
              </div>
              <div class="user-name">
                {{Auth::user()->name}}
              </div>
            </div>
          </div>

          <div class="col-lg-10">
            <div class="row">
              <table id="userDataTable" class="display" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>Match ID</th>
                          <th>Game Type</th>
                          <th>Number of Players</th>
                          <th>Start Time</th>
                      </tr>
                  </thead>
                  <tfoot>
                      <tr>
                          <th>Match ID</th>
                          <th>Game Type</th>
                          <th>Number of Players</th>
                          <th>Start Time</th>
                      </tr>
                  </tfoot>
              </table>
            </div>

            <div class="row">
              <div class="col-lg-10">

              </div>
            </div>
          </div>

      </div>
      <!-- // END BODY CONTAINER -->

    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
    
    <!-- Data Tables -->
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>


    <script type="text/javascript">
      $(document).ready(function() {

      var logicController = {

        apiURL: 'http://localhost:5000/',
        dataTable: {
          htmlElement: $("#userDataTable"),
          instance: null,
        },

        init: function() {

         this.loadData(this.apiURL);
          
        },

        loadData: function(apiURL) {
          $.ajax({
            context: this,
            type: 'GET',
            dataType: "json",
            url: this.apiURL + 'hist/' + {{Auth::user()->steamid32}},
            success: function(data) {
              console.log(data);

              var cleanData = [];

              $.each(data.matches, function(key, val) {
                cleanData.push([
                    val.match_id,
                    val.lobby_type,
                    val.players.length,
                    val.start_time
                  ]);
              });

              console.log(cleanData);

              // Initialize the data table and load data
              this.dataTable.instance = this.dataTable.htmlElement.DataTable({
                data: cleanData,
                columns: [
                  { title: 'Match ID' },
                  { title: 'Game Type' },
                  { title: 'Number of Players' },
                  { title: 'Start Time' }
                ],
              });

            },
          });
        },
        
        loadMatchInfo: function(matchId) {
          $.ajax({
            context: this,
            type: 'GET',
            dataType: "json",
            url: this.apiURL + 'match/' + matchId,
            success: function(data) {
              console.log(data);
            },
          });
        },

        getKDA: function(playerId, ) {

        }
      };

      logicController.init();
      //logicController.getkda({{Auth::user()->steamid32}});

      

      });
    </script>  
  </body>
</html>