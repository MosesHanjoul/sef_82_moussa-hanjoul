<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="{{ asset('css/welcome.css') }}" rel="stylesheet">

        <title>{{config('app.name')}}</title>

        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    </head>
    <body>
            <header class="header" id="header">
        <!--header-start-->
        <div class="container">
            <figure class="logo animated fadeInDown delay-07s">
                <a href="#"><img src="{{asset('img/wild_card.svg')}}" alt=""></a>
            </figure>
            <h1 class="animated fadeInDown delay-07s">Your Dota 2 Companion</h1>

            <a class="link animated fadeInUp delay-1s servicelink" href="auth/steam">Login</a>
        </div>
    </header>
<!-- 
            <img src="../img/wild_card.svg">
            <br />
            {{config('app.name')}} 
            @if(Auth::check())
                <img src="{{Auth::user()->avatarfull}}">
                <br />
                {{Auth::user()->name}}
                <br />
                <a href="logout">Logout</a>
                <br />
                @else
                <span class="btn btn-info">
                <a href="auth/steam">
                <img src="../img/steam_login_box.png" hidden>
                Click me
                </a>
                </span>
                @endif
            <div class="links">
                <a href="http://blog.dota2.com/">Dota2 News</a>
                <a href="http://store.steampowered.com/app/570/Dota_2/">Download Dota2</a>
            </div>
        </div>
    </div> -->
    </body>
</html>
