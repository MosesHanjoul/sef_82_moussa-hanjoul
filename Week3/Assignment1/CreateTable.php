<?php
class CreateTable 
{
	function __construct($string) 
	{
		$tableParameters=explode(',',$string);

		if(!is_file("$tableParameters[2].csv")) {
			$tableName=str_replace('"',"",$tableParameters[2]);
			$table=fopen("LocalDB/$tableName.csv","a+");
			array_splice($tableParameters,0,4);
			fputcsv($table,str_replace('"',"",$tableParameters));
			echo "Table \"$tableName\" Created\n";
		}else {
			echo "Table Already Exists !\n";
		}
	}

}
echo "Enter Table Name followed by column name/s:\ni.e. CREATE,TABLE,\"Tablename\",COLUMNS,\"column1\",\"column2\" etc. :\n";
$userInput=trim(fgets(STDIN));
$test=new CreateTable($userInput);