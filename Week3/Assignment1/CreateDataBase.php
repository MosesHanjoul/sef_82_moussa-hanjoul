<?php

class CreateDataBase 
{

	function __construct() 
	{
	}

	function NewDataBase($dataBaseName)
	{
		if(!is_dir("LocalDB/$dataBaseName")) {
			mkdir("LocalDB/$dataBaseName");
			echo "\"$dataBaseName\" CREATED\n";
		}else {
			echo "The \"$dataBaseName DATABASE\" already exists!\n";
		}	
	}
}
echo "Enter New DataBase Name: ";
$userInput=trim(fgets(STDIN));
$test=new CreateDataBase();
$test->NewDataBase($userInput);