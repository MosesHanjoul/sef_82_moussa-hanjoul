<?php
class AddRecord
{
	function __construct($string) 
	{
		$recordParameters=explode(',',$string);
		$fileName=str_replace('"',"",$recordParameters[1]);
		if(is_file("LocalDB/$fileName.csv")) {
			fopen("LocalDB/$fileName.csv","a");
			array_splice($recordParameters,0,3);
			fputcsv(fopen("LocalDB/$fileName.csv","a"),str_replace('"',"",$recordParameters));
			$record=implode(",",str_replace('','"',$recordParameters));
			echo "Record ADDED\n$record\n";
		}else {
			echo "Cannot add Record\nThis Table Doesn't Exist!\n";
		}
	}
}
echo "Enter TableName followed by Record:\ni.e. ADD,\"Tablename\",Record,\"12\",\"Moussa\" etc. :\n";
$userInput=trim(fgets(STDIN));
$test=new AddRecord($userInput);