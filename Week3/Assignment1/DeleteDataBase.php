<?php

class DeleteDataBase
{
	function __construct()
	{

	}
	function WipeDB($dataBaseName) 
	{
		if(is_dir("LocalDB/$dataBaseName")) {
		foreach(glob("LocalDB/$dataBaseName".'/*') as $existingFile ) {
			unlink($existingFile);
		}
		rmdir("LocalDB/$dataBaseName");
		echo "\"$dataBaseName\" DELETED!\n";
		}else {
			echo "Can't delete \"$dataBaseName\"\nThis DATABASE Doesn't Exist!\n";
		}
	}
}
echo "Enter DataBase NAME you want to delete: ";
$userInput=trim(fgets(STDIN));
$test=new DeleteDataBase();
$test->WipeDB($userInput);